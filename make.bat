@ECHO OFF

pushd %~dp0

REM Command file for Clicking

if "%1" == "" goto help
if "%1" == "wheel" goto wheel

:help
echo [31mClicking v0.2.0[0m
echo.Please specify what to make:
echo.  [94mwheel       [0mto build wheel distribution
goto end

:wheel
rm -rf build/
python setup.py bdist_wheel
goto end

:end
popd
