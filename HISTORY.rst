=======
History
=======


0.2.0
-----

* Add style module.
* Add colorized Group and Command.


0.1.1
-----

* Add exception that print message in bold red when raised.


0.1.0
-----

* First public release.
