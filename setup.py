from setuptools import setup, find_packages


with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

setup(
    name='clicking',
    version='0.2.0',
    description="Convenience function for Click.",
    long_description=readme + '\n\n' + history,
    url='https://gitlab.com/julot/clicking',
    author='Andy Yulius',
    author_email='andy.julot@gmail.com',
    keywords='clicking',
    install_requires=[
        'Click>=6.0',

        'click_help_colors>=0.5',
        'colorama>=0.4.1',
    ],
    extras_require={
        'dev': [
            'bumpversion',
            'flake8',
            'pip-review',
            'rope',
            'twine',
        ],
    },
    packages=find_packages(where='src'),
    package_dir={'': 'src'},
    include_package_data=True,
    zip_safe=True,
    license="MIT license",
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
)
