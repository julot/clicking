########
Clicking
########

|PyPI version| |PyPI pyversions| |PyPI license| |PyPI status| |PyPI format|

.. |PyPI version| image:: https://img.shields.io/pypi/v/clicking.svg
   :target: https://pypi.org/project/clicking

.. |PyPI pyversions| image:: https://img.shields.io/pypi/pyversions/clicking.svg
   :target: https://pypi.org/project/clicking

.. |PyPI license| image:: https://img.shields.io/pypi/l/clicking.svg
   :target: https://pypi.org/project/clicking

.. |PyPI status| image:: https://img.shields.io/pypi/status/clicking.svg
   :target: https://pypi.org/project/clicking

.. |PyPI format| image:: https://img.shields.io/pypi/format/clicking.svg
   :target: https://pypi.org/project/clicking


Convenience wrapper for `Click <http://click.pocoo.org>`__.


* Free software: MIT license


Installation
============

.. code-block:: bat

  (.venv) > pip install clicking


Usage
=====

Echo
----

Convenience functions for ``click.echo`` because it's a pain to remember the
color and naming convention I myself use before.

info
  Print message in bold white.

progress
  Print message in bold blue.

working
  Print message in bold cyan.

success
  Print message in bold green.

warning
  Print message in bold yellow.

fail
  Print message in bold red.


.. code-block:: python

   import clicking

   clicking.info('Hello world!')


Style
-----

Convenience functions for ``click.style`` due to the same above reason.

info
  Text in bold white.

progress
  Text in bold blue.

working
  Text in bold cyan.

success
  Text in bold green.

warning
  Text in bold yellow.

fail
  Text in bold red.

.. code-block:: python

   import clicking

   print(clicking.style.info('Hello world!'))


Exception
---------

Error
  Print message in bold red and exit.

.. code-block:: python

   import clicking

   raise clicking.Error('Something broke!')


Group
-----

Colorized custom class for ``click.group``.

.. code-block:: python

   import click
   import clicking


   @click.group(cls=clicking.Group)
   def group():
      pass


Command
-------

Colorized custom class for ``click.command``.

.. code-block:: python

   import click
   import clicking


   @click.command(cls=clicking.Command)
   def command():
      pass
