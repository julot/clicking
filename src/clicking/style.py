import click


def info(text):
    """Text in bold white."""
    return click.style(text=text, bold=True)


def progress(text):
    """Text in bold blue."""
    return click.style(text=text, fg='blue', bold=True)


def working(text):
    """Text in bold cyan."""
    return click.style(text=text, fg='cyan', bold=True)


def success(text):
    """Text in bold green."""
    return click.style(text=text, fg='green', bold=True)


def warning(text):
    """Text in bold yellow."""
    return click.style(text=text, fg='yellow', bold=True)


def fail(text):
    """Text in bold red."""
    return click.style(text=text, fg='red', bold=True)
