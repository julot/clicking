import sys
import click
import click_help_colors

from . import style


__title__ = 'Clicking'
__version__ = '0.2.0'
__description__ = 'Convenience function for Click.'
__author__ = 'Andy Yulius'
__email__ = 'andy.julot@gmail.com'
__copyright__ = '© 2017-2018 Andy Yulius <andy.julot@gmail.com>'


def info(message, nl=True):
    """Print message in bold white."""
    if sys.stdout.isatty():
        return click.echo(message=style.info(message), nl=nl)


def progress(message, nl=True):
    """Print message in bold blue."""
    if sys.stdout.isatty():
        return click.echo(message=style.progress(message), nl=nl)


def working(message, nl=True):
    """Print message in bold cyan."""
    if sys.stdout.isatty():
        return click.echo(message=style.working(message), nl=nl)


def success(message, nl=True):
    """Print message in bold green."""
    if sys.stdout.isatty():
        return click.echo(message=style.success(message), nl=nl)


def warning(message, nl=True):
    """Print message in bold yellow."""
    return click.echo(message=style.warning(message), nl=nl)


def fail(message, nl=True):
    """Print message in bold red."""
    return click.echo(message=style.fail(message), nl=nl)


class Error(click.ClickException):
    """Print an exception in bold red."""
    def show(self, file=None):
        fail('Error: %s' % self.format_message())


class _ColorMixin(object):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.help_headers_color = 'yellow'
        self.help_options_color = 'green'


class Group(_ColorMixin, click_help_colors.HelpColorsGroup):
    """Group with color."""


class Command(_ColorMixin, click_help_colors.HelpColorsCommand):
    """Command with color."""
